use heapless;
use serde::{Deserialize, Serialize};
#[derive(Deserialize, Serialize)]
pub(crate) struct MqttState {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub temperature: Option<f32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub humidity: Option<f32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub atmospheric_pressure: Option<f32>,
}

#[derive(Deserialize, Serialize)]
pub(crate) enum DeviceClass {
    #[serde(rename = "apparent_power")]
    ApparentPower,
    #[serde(rename = "aqi")]
    AirQualityIndex,
    #[serde(rename = "atmospheric_pressure")]
    AtmosphericPressure,
    #[serde(rename = "battery")]
    Battery,
    #[serde(rename = "carbon_dioxide")]
    CarbonDioxide,
    #[serde(rename = "carbon_monoxide")]
    CarbonMonoxide,
    #[serde(rename = "current")]
    Current,
    #[serde(rename = "data_rate")]
    DataRate,
    #[serde(rename = "data_size")]
    DataSize,
    /// Date string, ISO 8601
    #[serde(rename = "date")]
    Date,
    #[serde(rename = "distance")]
    Distance,
    #[serde(rename = "duration")]
    Duration,
    #[serde(rename = "energy")]
    Energy,
    #[serde(rename = "energy_storage")]
    EnergyStorage,
    #[serde(rename = "enum")]
    Enum,
    #[serde(rename = "frequency")]
    Frequency,
    #[serde(rename = "gas")]
    GasVolume,
    #[serde(rename = "humidity")]
    Humidity,
    #[serde(rename = "illuminance")]
    Illuminance,
    #[serde(rename = "irradiance")]
    Irradiance,
    #[serde(rename = "moisture")]
    Moisture,
    /// ISO 4217
    #[serde(rename = "monetary")]
    Monetary,
    #[serde(rename = "nitrogen_dioxide")]
    NitrogenDioxide,
    #[serde(rename = "nitrogen_monoxide")]
    NitrogenMonoxide,
    #[serde(rename = "nitrous_oxide")]
    NitrousOxide,
    #[serde(rename = "ozone")]
    Ozone,
    #[serde(rename = "ph")]
    Ph,
    #[serde(rename = "pm1")]
    Pm1,
    #[serde(rename = "pm25")]
    Pm2_5,
    #[serde(rename = "pm10")]
    Pm10,
    #[serde(rename = "power_factor")]
    PowerFactor,
    #[serde(rename = "power")]
    Power,
    #[serde(rename = "precipitation")]
    Precipitation,
    #[serde(rename = "precipitation_intensity")]
    PrecipitationIntensity,
    #[serde(rename = "pressure")]
    Pressure,
    #[serde(rename = "reactive_power")]
    ReactivePower,
    #[serde(rename = "signal_strength")]
    SignalStrength,
    #[serde(rename = "sound_pressure")]
    SoundPressure,
    #[serde(rename = "speed")]
    Speed,
    #[serde(rename = "temperature")]
    Temperature,
    #[serde(rename = "timestamp")]
    Timestamp,
    #[serde(rename = "volatile_organic_compounds")]
    VolatileOrganicCompounds,
    #[serde(rename = "volatile_organic_compounds_parts")]
    VolatileOrganicCompoundsParts,
    #[serde(rename = "voltage")]
    Voltage,
    #[serde(rename = "volume")]
    Volume,
    #[serde(rename = "volume_storage")]
    StoredVolume,
    #[serde(rename = "water")]
    WaterConsumption,
    #[serde(rename = "weight")]
    Weight,
    #[serde(rename = "wind_speed")]
    WindSpeed,
}

#[derive(Deserialize, Serialize)]
pub(crate) enum StateClass {
    #[serde(rename = "measurement")]
    Measurement,
    #[serde(rename = "total")]
    Total,
    #[serde(rename = "total_increasing")]
    TotalIncreasing,
}

#[derive(Serialize)]
pub(crate) struct DeviceDiscoveryDevice<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hw_version: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sw_version: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub model: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub manufacturer: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identifiers: Option<&'a [&'a str]>,
}

#[derive(Serialize)]
pub(crate) struct DeviceDiscovery<'a> {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<heapless::String<32>>,
    pub device_class: DeviceClass,
    pub state_topic: heapless::String<64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_class: Option<StateClass>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unit_of_measurement: Option<heapless::String<8>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub value_template: Option<heapless::String<64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unique_id: Option<heapless::String<32>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expire_after: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub device: Option<&'a DeviceDiscoveryDevice<'a>>,
}
