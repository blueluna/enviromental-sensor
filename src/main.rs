#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

mod home_assistant;

use bme280::i2c::BME280;
use core::{str::FromStr, cell::RefCell};
use defmt;
use embassy_executor::Spawner;
use embassy_net::{dns::DnsQueryType, tcp::TcpSocket, Config, Stack, StackResources};
use embassy_time::{Delay, Duration, Timer};
use embedded_svc::wifi::{ClientConfiguration, Configuration, Wifi};
use esp_backtrace as _;
use esp_wifi::wifi::{WifiController, WifiDevice, WifiEvent, WifiStaDevice, WifiState};
use esp_wifi::{self, EspWifiInitFor};
use hal::{clock::ClockControl, embassy, peripherals::Peripherals, prelude::*, timer::TimerGroup, i2c::I2C, gpio::IO};
use hal::{systimer::SystemTimer, Rng};
use embedded_hal::i2c::I2c;
use rust_mqtt;
use serde_json_core;
use static_cell::make_static;
use tmp117::Tmp117;
use ufmt::uwrite;
use embedded_hal_bus;

const SSID: &str = env!("SSID");
const PASSWORD: &str = env!("PASSWORD");
const MQTT_HOST: &str = env!("MQTT_HOST");
const MQTT_USERNAME: &str = env!("MQTT_USERNAME");
const MQTT_PASSWORD: &str = env!("MQTT_PASSWORD");
const MQTT_BUFFER_SIZE: usize = 1024;

const DEVICE_NAME: &'static str = "ErikB";

#[embassy_executor::task]
async fn connection(mut controller: WifiController<'static>) {
    loop {
        match esp_wifi::wifi::get_wifi_state() {
            WifiState::StaConnected => {
                // wait until we're no longer connected
                controller.wait_for_event(WifiEvent::StaDisconnected).await;
                defmt::warn!("Lost IEEE802.11");
                Timer::after(Duration::from_millis(5000)).await
            }
            _ => {}
        }
        if !matches!(controller.is_started(), Ok(true)) {
            let client_config = Configuration::Client(ClientConfiguration {
                ssid: SSID.try_into().unwrap(),
                password: PASSWORD.try_into().unwrap(),
                ..Default::default()
            });
            controller.set_configuration(&client_config).unwrap();
            controller.start().await.unwrap();
        }

        match controller.connect().await {
            Ok(_) => (),
            Err(e) => {
                defmt::error!("Failed to connect to wifi: {:?}", e);
                Timer::after(Duration::from_millis(5000)).await
            }
        }
    }
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    stack.run().await
}

async fn mqtt_send<
    T: serde::ser::Serialize,
    IO: embedded_io_async::Read + embedded_io_async::Write,
    const MAX_PROPERTIES: usize,
    RNG: rand_core::RngCore,
>(
    client: &mut rust_mqtt::client::client::MqttClient<'_, IO, MAX_PROPERTIES, RNG>,
    topic: &str,
    value: &T,
    retain: bool,
) -> Result<(), rust_mqtt::packet::v5::reason_codes::ReasonCode> {
    let mut payload = [0; MQTT_BUFFER_SIZE];
    let bytes = match serde_json_core::ser::to_slice(value, &mut payload) {
        Ok(size) => size,
        Err(_) => {
            defmt::error!("Failed to serialize {}", topic);
            return Err(rust_mqtt::packet::v5::reason_codes::ReasonCode::PayloadFormatInvalid);
        }
    };
    match client
        .send_message(
            topic,
            &payload[..bytes],
            rust_mqtt::packet::v5::publish_packet::QualityOfService::QoS0,
            retain,
        )
        .await
    {
        Ok(()) => Ok(()),
        Err(mqtt_error) => match mqtt_error {
            rust_mqtt::packet::v5::reason_codes::ReasonCode::NetworkError => {
                defmt::error!("MQTT Network Error");
                Err(mqtt_error)
            }
            _ => {
                defmt::error!("Other MQTT Error: {:?}", mqtt_error);
                Err(mqtt_error)
            }
        },
    }
}

#[main]
async fn main(spawner: Spawner) -> ! {
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();

    let clocks = ClockControl::max(system.clock_control).freeze();
    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);

    // Instantiate the Random Number Generator peripheral:
    let mut rng = Rng::new(peripherals.RNG);
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    let sck = io.pins.gpio2;
    let sda = io.pins.gpio3;
    let i2c_device = I2C::new(
        peripherals.I2C0,
        sda,
        sck,
        100u32.kHz(),
        &clocks,
    );
    let i2c_ref = RefCell::new(i2c_device);
    let mut bme280 = BME280::new_primary(embedded_hal_bus::i2c::RefCellDevice::new(&i2c_ref));
    let mut tmp = Tmp117::<0x48, _, _>::new(embedded_hal_bus::i2c::RefCellDevice::new(&i2c_ref));
    let mut i2c_2 = embedded_hal_bus::i2c::RefCellDevice::new(&i2c_ref);

    let seed = u64::from(rng.random()) | (u64::from(rng.random()) << 32);

    let timer = SystemTimer::new(peripherals.SYSTIMER).alarm0;
    let init = esp_wifi::initialize(
        EspWifiInitFor::Wifi,
        timer,
        rng,
        system.radio_clock_control,
        &clocks,
    )
    .unwrap();

    let wifi = peripherals.WIFI;
    let (wifi_interface, controller) =
        esp_wifi::wifi::new_with_mode(&init, wifi, WifiStaDevice).unwrap();

    embassy::init(&clocks, timer_group0);

    let _ = tmp.reset(&mut Delay);


    let bme280_config = bme280::Configuration::default()
        .with_humidity_oversampling(bme280::Oversampling::Oversampling1X)
        .with_pressure_oversampling(bme280::Oversampling::Oversampling1X)
        .with_temperature_oversampling(bme280::Oversampling::Oversampling1X)
        .with_iir_filter(bme280::IIRFilter::Off);
    if let Err(e) = bme280.init_with_config(&mut Delay, bme280_config) {
        defmt::error!("Failed to setup BME280, {:?}", e);
    }
    match bme280.measure(&mut Delay) {
        Ok(_) => {
        }
        Err(error) => {
            defmt::error!("BME280: {:?}", error);
        }
    }

    let _ = i2c_2.write(0x40, &[0xfe]);
    defmt::info!("Reset si7021");

    let config = Config::dhcpv4(Default::default());

    let mac_address = wifi_interface.mac_address();

    let device_id = {
        let mut device_id: heapless::String<12> = heapless::String::new();
        let _ = uwrite!(
            device_id,
            "{:02x}{:02x}{:02x}{:02x}{:02x}{:02x}",
            mac_address[0],
            mac_address[1],
            mac_address[2],
            mac_address[3],
            mac_address[4],
            mac_address[5]
        );
        device_id
    };

    let topic_state = {
        let mut topic: heapless::String<64> = heapless::String::new();
        let _ = uwrite!(topic, "homeassistant/sensor/{}/state", device_id.as_str());
        topic
    };

    let topic_discovery_temperature = {
        let mut topic: heapless::String<64> = heapless::String::new();
        let _ = uwrite!(
            topic,
            "homeassistant/sensor/{}_temperature/config",
            device_id.as_str()
        );
        topic
    };

    let topic_discovery_humidity = {
        let mut topic: heapless::String<64> = heapless::String::new();
        let _ = uwrite!(
            topic,
            "homeassistant/sensor/{}_humidity/config",
            device_id.as_str()
        );
        topic
    };

    let topic_discovery_atmospheric_pressure = {
        let mut topic: heapless::String<64> = heapless::String::new();
        let _ = uwrite!(
            topic,
            "homeassistant/sensor/{}_atmospheric_pressure/config",
            device_id.as_str()
        );
        topic
    };

    let client_identity = {
        let mut identity: heapless::String<32> = heapless::String::new();
        let _ = uwrite!(identity, "awwww-{}", device_id.as_str());
        identity
    };

    // Init network stack
    let stack = &*make_static!(Stack::new(
        wifi_interface,
        config,
        make_static!(StackResources::<3>::new()),
        seed
    ));

    let _ = spawner.spawn(connection(controller));
    let _ = spawner.spawn(net_task(&stack));
    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 4096];

    loop {
        if stack.is_link_up() {
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }

    loop {
        if let Some(config) = stack.config_v4() {
            defmt::info!("IPv4: {}", config.address);
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }

    loop {
        Timer::after(Duration::from_millis(1_000)).await;

        let mut socket = TcpSocket::new(&stack, &mut rx_buffer, &mut tx_buffer);

        socket.set_timeout(Some(embassy_time::Duration::from_secs(60)));

        let mqtt_host = MQTT_HOST;
        let mqtt_address = match stack.dns_query(mqtt_host, DnsQueryType::A).await {
            Ok(responses) => {
                for address in responses.iter() {
                    defmt::info!("{} -> {}", mqtt_host, address);
                }
                if responses.len() > 0 {
                    responses[0]
                } else {
                    defmt::error!("DNS query no address");
                    Timer::after(Duration::from_secs(60)).await;
                    continue;
                }
            }
            Err(e) => {
                defmt::error!("DNS query error: {:?}", e);
                Timer::after(Duration::from_secs(60)).await;
                continue;
            }
        };

        let remote_endpoint = (mqtt_address, 1883);
        let result = socket.connect(remote_endpoint).await;
        if let Err(e) = result {
            defmt::error!("connect error: {:?}", e);
            continue;
        }
        let mut mqtt_config = rust_mqtt::client::client_config::ClientConfig::new(
            rust_mqtt::client::client_config::MqttVersion::MQTTv5,
            rust_mqtt::utils::rng_generator::CountingRng(20000),
        );
        mqtt_config
            .add_max_subscribe_qos(rust_mqtt::packet::v5::publish_packet::QualityOfService::QoS1);
        mqtt_config.add_client_id(client_identity.as_str());
        mqtt_config.max_packet_size = MQTT_BUFFER_SIZE as u32;
        mqtt_config.keep_alive = 10;
        if !MQTT_USERNAME.is_empty() && !MQTT_PASSWORD.is_empty() {
            mqtt_config.add_username(MQTT_USERNAME);
            mqtt_config.add_password(MQTT_PASSWORD);
        }
        let mut recv_buffer = [0; MQTT_BUFFER_SIZE];
        let mut write_buffer = [0; MQTT_BUFFER_SIZE];

        let mut mqtt_client = rust_mqtt::client::client::MqttClient::<_, 5, _>::new(
            socket,
            &mut write_buffer,
            MQTT_BUFFER_SIZE,
            &mut recv_buffer,
            MQTT_BUFFER_SIZE,
            mqtt_config,
        );

        match mqtt_client.connect_to_broker().await {
            Ok(()) => {}
            Err(mqtt_error) => match mqtt_error {
                rust_mqtt::packet::v5::reason_codes::ReasonCode::NetworkError => {
                    defmt::error!("MQTT Network Error");
                    continue;
                }
                _ => {
                    defmt::error!("Other MQTT Error: {:?}", mqtt_error);
                    continue;
                }
            },
        }

        let identifiers = [device_id.as_str()];
        let device = home_assistant::DeviceDiscoveryDevice {
            name: Some(DEVICE_NAME),
            manufacturer: Some("ERIK of Sweden"),
            model: Some("ERBÅ ESP32-C3 TMP117"),
            hw_version: None,
            sw_version: None,
            identifiers: Some(&identifiers),
        };

        {
            let unique_id = {
                let mut identity: heapless::String<32> = heapless::String::new();
                let _ = uwrite!(identity, "{}_temperature", device_id.as_str());
                identity
            };
            let config = home_assistant::DeviceDiscovery {
                name: None,
                device_class: home_assistant::DeviceClass::Temperature,
                state_topic: topic_state.clone(),
                state_class: Some(home_assistant::StateClass::Measurement),
                unit_of_measurement: Some(heapless::String::from_str("°C").unwrap()),
                value_template: Some(
                    heapless::String::from_str("{{ value_json.temperature }}").unwrap(),
                ),
                unique_id: Some(unique_id),
                expire_after: Some(3600),
                device: Some(&device),
            };
            let _ = mqtt_send(
                &mut mqtt_client,
                topic_discovery_temperature.as_str(),
                &config,
                true,
            )
            .await;
        }

        {
            let unique_id = {
                let mut identity: heapless::String<32> = heapless::String::new();
                let _ = uwrite!(identity, "{}_humidity", device_id.as_str());
                identity
            };
            let config = home_assistant::DeviceDiscovery {
                name: None,
                device_class: home_assistant::DeviceClass::Humidity,
                state_topic: topic_state.clone(),
                state_class: Some(home_assistant::StateClass::Measurement),
                unit_of_measurement: Some(heapless::String::from_str("%").unwrap()),
                value_template: Some(
                    heapless::String::from_str("{{ value_json.humidity }}").unwrap(),
                ),
                unique_id: Some(unique_id),
                expire_after: Some(3600),
                device: Some(&device),
            };
            let _ = mqtt_send(
                &mut mqtt_client,
                topic_discovery_humidity.as_str(),
                &config,
                true,
            )
            .await;
        }

        {
            let unique_id = {
                let mut identity: heapless::String<32> = heapless::String::new();
                let _ = uwrite!(identity, "{}_atmospheric_pressure", device_id.as_str());
                identity
            };
            let config = home_assistant::DeviceDiscovery {
                name: None,
                device_class: home_assistant::DeviceClass::AtmosphericPressure,
                state_topic: topic_state.clone(),
                state_class: Some(home_assistant::StateClass::Measurement),
                unit_of_measurement: Some(heapless::String::from_str("Pa").unwrap()),
                value_template: Some(
                    heapless::String::from_str("{{ value_json.atmospheric_pressure }}").unwrap(),
                ),
                unique_id: Some(unique_id),
                expire_after: Some(3600),
                device: Some(&device),
            };
            let _ = mqtt_send(
                &mut mqtt_client,
                topic_discovery_atmospheric_pressure.as_str(),
                &config,
                true,
            )
            .await;
        }

        let mut mqtt_state = home_assistant::MqttState {
            temperature: None,
            humidity: None,
            atmospheric_pressure: None,
        };

        'sensor_loop: loop {

            let temperature = if let Ok(temperature) = tmp.oneshot_delay(tmp117::register::Average::NoAverage, &mut Delay).await {
                Some(temperature)
            } else {
                defmt::error!("TMP117 read timeout");
                let _ = tmp.reset(&mut Delay);
                None
            };
            match temperature {
                Some(temperature) => {
                    defmt::info!("TMP117: {=f32}°C", temperature);
                }
                None => {
                    defmt::warn!("TMP117: nothing");
                }
            }
            mqtt_state.temperature = temperature;

            match bme280.measure(&mut Delay) {
                Ok(measurement) => {
                    mqtt_state.humidity = Some(measurement.humidity);
                    mqtt_state.atmospheric_pressure = Some(measurement.pressure);
                    defmt::info!("BME280: {=f32}°C {=f32}% {=f32}Pa", measurement.temperature, measurement.humidity, measurement.pressure);
                }
                Err(error) => {
                    defmt::error!("BME280: {:?}", error);
                    mqtt_state.humidity = None;
                    mqtt_state.atmospheric_pressure = None;
                }
            }

            match i2c_2.write(0x40, &[0xe5]) {
                Ok(()) => {
                    Timer::after(Duration::from_millis(23)).await;
                    let mut data = [0; 2];
                    let mut humidity_code = 0xffffu16;
                    let mut temperature_code = 0xffffu16;
                    match i2c_2.read(0x40, data.as_mut_slice()) {
                        Ok(()) => {
                            humidity_code = u16::from_be_bytes(data);
                        }
                        Err(error) => {
                            defmt::error!("SI7021: read RH error {:?}", error);
                        }
                    }
                    match i2c_2.write_read(0x40, &[0xe0], data.as_mut_slice()) {
                        Ok(()) => {
                            temperature_code = u16::from_be_bytes(data);
                        }
                        Err(error) => {
                            defmt::error!("SI7021: read error {:?}", error);
                        }
                    }
                    let humidity = ((f32::from(humidity_code) * 125.0) / 65536.0) - 6.0;
                    let temperature = (((f32::from(temperature_code) * 17572.0) / 65536.0) - 4685.0) / 100.0;
                    defmt::info!("SI7021: {=f32}°C {=f32}%", temperature, humidity);
                }
                Err(error) => {
                    defmt::error!("SI7021: measure error {:?}", error);
                }
            }

            match mqtt_send(&mut mqtt_client, topic_state.as_str(), &mqtt_state, false).await {
                Ok(_) => (),
                Err(code) => {
                    match code {
                        rust_mqtt::packet::v5::reason_codes::ReasonCode::NetworkError => {
                            if !stack.is_config_up() {
                                defmt::error!("No network stack");
                            }
                            break;
                        }
                        _ => (),
                    }
                }
            }
            for _ in 0..12 {
                match mqtt_client.send_ping().await {
                    Ok(()) => (),
                    Err(e) => {
                        defmt::error!("Ping error, {}", e);
                        break 'sensor_loop;
                    }
                }
                Timer::after(Duration::from_secs(5)).await;
            }
        }
    }
}
