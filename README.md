# Anywhere World Weather Wire

## Environmental Sensor

This is the firmware for a ESP32-C3 based environmental sensor. A Espressif ESP32-C3-DevKitM-1 was used.
Expects I2C device(s) on a I2C bus hooked up with SCK  to pin 2 and SDA to pin 3. Following sensors are expected,
 - BME280 sensor with I2C address 0x76.
 - si7021 sensor with I2C address 0x40.
 - TMP117 sensor with I2C address 0x48.

The firmware will connect to Wi-Fi, open a MQTT connection, send Home Assistant MQTT discovery.
After setup, the measured values are sent every minute.

### Build

Requires a fairly modern version of Rust. See https://rustup.rs for installation.

Some configuration is applied from environment variables during the build.
- SSID, Wi-Fi access point SSID
- PASSWORD, Wi-Fi access point password
- MQTT_HOST, address of the MQTT broker
- MQTT_USERNAME, Username for the MQTT broker, can be empty
- MQTT_PASSWORD, Password for the MQTT broker, can be empty

```shell
SSID=<SSID> PASSWORD=<PASSWORD> MQTT_HOST=<ADDRESS> MQTT_USERNAME=<USERNAME> MQTT_PASSWORD=<PASSWORD> cargo build
```

### Flash and run

Running the firmware on target requires espflash. See https://crates.io/crates/cargo-espflash. Using the git version
```shell
cargo install -f --git https://github.com/esp-rs/espflash espflash
```

Run similar to the build step.

```shell
SSID=<SSID> PASSWORD=<PASSWORD> MQTT_HOST=<ADDRESS> MQTT_USERNAME=<USERNAME> MQTT_PASSWORD=<PASSWORD> cargo run
```
